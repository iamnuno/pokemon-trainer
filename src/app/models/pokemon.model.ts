export interface Pokemon {
  id: string;
  image: string;
  types?: Array<string>;
  hp?: string;
  attack?: string;
  defense?: string;
  specialAttack?: string;
  specialDefense?: string;
  speed?: string;
  name: string;
  height?: number;
  weight?: number;
  abilities?: Array<string>;
  xp?: string;
  moves?: Array<string>;
}
