import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/shared/services/pokemon.service';
import { retry } from 'rxjs/operators';

// shows collected pokemons
@Component({
  selector: 'app-trainer-home',
  templateUrl: './trainer-home.component.html',
  styleUrls: ['./trainer-home.component.css'],
})
export class TrainerHomeComponent implements OnInit {
  pokemons: Array<Pokemon>;
  isEmpty: boolean;

  constructor(private pokemonService: PokemonService) {
    this.pokemons = [];
    this.isEmpty = false;
  }

  ngOnInit(): void {
    const items = { ...localStorage };
    // filter pokemons only and not the user name
    const pokemonsInStorage = Object.entries(items).filter(
      ([key, value]) => !(key === 'data' || key === 'randid') // randid is added by (adBlocker) extension to local storage
    );

    if (pokemonsInStorage.length === 0) {
      this.isEmpty = true;
      return;
    }

    for (const id of pokemonsInStorage) {
      this.pokemonService
        .getPokemon(id[1].toString())
        .subscribe((data: any) => {
          const {
            ['sprites']: {
              ['other']: { ['official-artwork']: artwork },
            },
          } = data;
          const pokemon: Pokemon = {
            id: data.id,
            name: data.name,
            image: artwork.front_default,
          };
          this.pokemons.push(pokemon);
          this.pokemons.sort((a, b) => parseInt(a.id, 10) - parseInt(b.id, 10));
        });
    }
  }
}
