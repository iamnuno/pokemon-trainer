import { NgModule } from '@angular/core';
import { TrainerRoutingModule } from './trainer-routing.module';
import { TrainerHomeComponent } from './trainer-home/trainer-home.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TrainerHomeComponent],
  imports: [TrainerRoutingModule, SharedModule],
})
export class TrainerModule {}
