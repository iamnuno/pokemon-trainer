import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// service responsible for fetching pokemon info from PokeAPI
// used by: pokemon-details, trainer-home and pokemons-home
@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = 'https://pokeapi.co/api/v2/pokemon/';
  }

  getPokemon(id: string): any {
    return this.http.get(this.apiUrl + `${id}`);
  }
}
