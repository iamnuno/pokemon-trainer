import { Injectable } from '@angular/core';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  public saveToStorage(user: User): void {
    localStorage.setItem('data', JSON.stringify(user));
  }
  public getUsername(): string {
    const data: string | null = localStorage.getItem('data');

    if (data) {
      const user: User = JSON.parse(data);
      const name: string | undefined = user.username;
      return name ? name : 'no user found';
    }
    return 'no user found';
  }
}
