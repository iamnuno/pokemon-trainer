import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LocalStorageService } from './localStorage.service';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private readonly localStorageService: LocalStorageService) {}

  public isLoggedIn$(): Observable<boolean> {
    return of(localStorage.getItem('data') !== null);
  }

  public login$(name: string): Observable<User> {
    const user: User = { username: name, pokemons: [] };
    this.localStorageService.saveToStorage(user);
    return of(user);
  }

  public clickable$(input: string): Observable<boolean> {
    if (input.replace(/\s/g, '').length === 0) {
      return of(true);
    } else {
      return of(false);
    }
  }
}
