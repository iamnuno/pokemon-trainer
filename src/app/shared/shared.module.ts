import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { CatalogueItemComponent } from './catalogue-item/catalogue-item.component';

@NgModule({
  declarations: [CatalogueComponent, CatalogueItemComponent],
  imports: [CommonModule],
  exports: [CommonModule, CatalogueComponent, CatalogueItemComponent],
})
export class SharedModule {}
