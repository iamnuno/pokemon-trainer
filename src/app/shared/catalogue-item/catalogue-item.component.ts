import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';

// individual pokemon card
@Component({
  selector: 'app-catalogue-item',
  templateUrl: './catalogue-item.component.html',
  styleUrls: ['./catalogue-item.component.css'],
})
export class CatalogueItemComponent implements OnInit {
  @Input() pokemon: Pokemon;
  // decide if pokemons details need to be shown or name and image are enough
  // pokemon-details sets it to true
  @Input() showDetails: boolean;

  constructor(private router: Router) {
    this.showDetails = false;
    this.pokemon = {
      id: '',
      image: '',
      name: '',
      types: [],
      height: 0,
      weight: 0,
      abilities: [],
      xp: '',
      moves: [],
    };
  }

  navigateToPokemonDetails(): void {
    this.router.navigate(['/pokemons', this.pokemon.id]);
  }

  onCollectClick(): void {
    localStorage.setItem(this.pokemon.name, this.pokemon.id);
  }

  ngOnInit(): void {}
}
