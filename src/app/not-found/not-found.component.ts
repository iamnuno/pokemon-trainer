import { Component, OnInit } from '@angular/core';
import { HomeFacade } from '../home/home.facade';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css'],
})
export class NotFoundComponent implements OnInit {
  constructor(private readonly homeFacade: HomeFacade) {}

  ngOnInit(): void {
    this.homeFacade.updateHeader();
  }
}
