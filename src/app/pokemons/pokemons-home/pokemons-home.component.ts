import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/shared/services/pokemon.service';
import { faBackward, faForward } from '@fortawesome/free-solid-svg-icons';

// used to display pokemons catalogue
@Component({
  selector: 'app-pokemons-home',
  templateUrl: './pokemons-home.component.html',
  styleUrls: ['./pokemons-home.component.css'],
})
export class PokemonsHomeComponent implements OnInit {
  pokemons: Array<Pokemon>;
  startAt: number;
  limit: number;
  endAt: number;
  forward = faForward;
  backward = faBackward;

  constructor(private pokemonService: PokemonService) {
    this.pokemons = [];
    this.startAt = 1;
    this.limit = 6; // show 6 pokemons at a time
    this.endAt = this.startAt + this.limit - 1;
  }

  ngOnInit(): void {
    this.getPokemons(this.startAt, this.endAt);
  }

  showNext(): void {
    this.startAt += this.limit;
    this.endAt = this.startAt + this.limit - 1;
    this.getPokemons(this.startAt, this.endAt);
  }

  showPrevious(): void {
    this.startAt -= this.limit;
    if (this.startAt < 1) {
      this.startAt = 1;
    }
    this.endAt = this.startAt + this.limit - 1;
    this.getPokemons(this.startAt, this.endAt);
  }

  getPokemons(startAt: number, endAt: number): void {
    this.pokemons = [];
    for (let id = startAt; id <= endAt; id++) {
      this.pokemonService.getPokemon(id.toString()).subscribe((data: any) => {
        const {
          ['sprites']: {
            ['other']: { ['official-artwork']: value },
          },
        } = data;

        const pokemon: Pokemon = {
          id: data.id,
          name: data.name,
          image: value.front_default,
        };
        this.pokemons.push(pokemon);
        this.pokemons.sort((a, b) => parseInt(a.id, 10) - parseInt(b.id, 10));
      });
    }
  }
}
