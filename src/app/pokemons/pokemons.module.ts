import { NgModule } from '@angular/core';
import { PokemonsRoutingModule } from './pokemons-routing.module';
import { PokemonsHomeComponent } from './pokemons-home/pokemons-home.component';
import { SharedModule } from '../shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [PokemonsHomeComponent],
  imports: [PokemonsRoutingModule, SharedModule, FontAwesomeModule],
})
export class PokemonsModule {}
