import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonsHomeComponent } from './pokemons-home/pokemons-home.component';

const routes: Routes = [
  {
    path: '',
    component: PokemonsHomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PokemonsRoutingModule {}
