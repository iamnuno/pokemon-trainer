import { Injectable } from '@angular/core';
import { LoginState } from './state/login.state';
import { Observable } from 'rxjs';
import { LoginService } from '../shared/services/login.service';

@Injectable({
  providedIn: 'root',
})
export class LoginFacade {
  constructor(
    private readonly loginService: LoginService,
    private readonly loginState: LoginState
  ) {}

  public clickable$(): Observable<boolean> {
    return this.loginState.getClickable$();
  }

  /**
   * updates the variable `clickable` when the user change the input
   * @param input user input
   */
  public inputHandler(input: string): void {
    this.loginService.clickable$(input).subscribe((state) => {
      this.loginState.setClickable(state);
    });
  }

  /**
   * save username to localstorage
   */
  public login(username: string): void {
    this.loginService.login$(username);
  }
}
