import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginFacade } from '../login.facade';
import { Observable } from 'rxjs';
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-login-home',
  templateUrl: './login-home.component.html',
  styleUrls: ['./login-home.component.css'],
})
export class LoginHomeComponent {
  name: string;
  faSignInAlt = faSignInAlt;

  constructor(
    private router: Router,
    private readonly loginFacade: LoginFacade
  ) {
    this.name = '';
  }

  // Getters
  get clickable$(): Observable<boolean> {
    return this.loginFacade.clickable$();
  }

  // EVENT HANDLERS
  onNameChange(name: string): void {
    this.name = name;
    this.loginFacade.inputHandler(this.name);
  }

  onLoginClick(): void {
    this.loginFacade.login(this.name);
    this.router.navigate(['/pokemons']);
  }
}
