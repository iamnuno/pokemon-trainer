import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginState {
  private readonly clickable$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    true
  );

  public getClickable$(): Observable<boolean> {
    return this.clickable$.asObservable();
  }

  public setClickable(state: boolean): void {
    this.clickable$.next(state);
  }
}
