import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor(
    private readonly location: Location,
    private readonly router: Router
  ) {}
  ngOnInit(): void {
    if (this.location.path() === '') {
      this.router.navigate(['login']);
    }
  }
}
