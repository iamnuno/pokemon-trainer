import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { LoginService } from '../shared/services/login.service';
import { HeaderState } from '../header/state/header.state';

@Injectable({
  providedIn: 'root',
})
export class RouteGuard implements CanActivate {
  private isLoggedIn: boolean;

  constructor(
    private readonly loginService: LoginService,
    private router: Router,
    private headerState: HeaderState
  ) {
    this.isLoggedIn = false;
  }
  public canActivate(
    route: ActivatedRouteSnapshot,
    status: RouterStateSnapshot
  ): boolean {
    this.loginService.isLoggedIn$().subscribe((state) => {
      if (state) {
        // redirect to home if user is already logged in
        if (status.url === '/login') {
          this.isLoggedIn = !state;
          this.router.navigate(['pokemons']);
        } else {
          this.isLoggedIn = state;
        }
      } else {
        // allow user to access login page if he is not logged in yet
        if (status.url !== '/login') {
          this.isLoggedIn = state;
          this.router.navigate(['']);
        } else {
          this.isLoggedIn = !state;
        }
      }
      this.headerState.setIsLoggedIn(state);
    });

    return this.isLoggedIn;
  }
}
