import { Injectable } from '@angular/core';
import { HeaderState } from '../header/state/header.state';
import { LoginService } from '../shared/services/login.service';

@Injectable({
  providedIn: 'root',
})
export class HomeFacade {
  constructor(
    private readonly headerState: HeaderState,
    private readonly loginService: LoginService
  ) {}

  /**
   * updates navbar state
   */
  public updateHeader(): void {
    this.loginService.isLoggedIn$().subscribe((state) => {
      this.headerState.setIsLoggedIn(state);
    });
  }
}
