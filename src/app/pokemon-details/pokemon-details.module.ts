import { NgModule } from '@angular/core';

import { PokemonDetailsRoutingModule } from './pokemon-details-routing.module';
import { PokemonDetailsHomeComponent } from './pokemon-details-home/pokemon-details-home.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PokemonDetailsHomeComponent],
  imports: [PokemonDetailsRoutingModule, SharedModule],
})
export class PokemonDetailsModule {}
