import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/shared/services/pokemon.service';

// used to display pokemon details
@Component({
  selector: 'app-pokemon-details-home',
  templateUrl: './pokemon-details-home.component.html',
  styleUrls: ['./pokemon-details-home.component.css'],
})
export class PokemonDetailsHomeComponent implements OnInit {
  id: string | null;
  pokemon: Pokemon;

  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService
  ) {
    this.id = '';
    this.pokemon = {
      id: '',
      image: '',
      name: '',
      types: [],
      height: 0,
      weight: 0,
      abilities: [],
      xp: '',
      moves: [],
      hp: '',
    };
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });

    if (this.id) {
      this.pokemonService.getPokemon(this.id).subscribe((data: any) => {
        const {
          ['sprites']: {
            ['other']: { ['official-artwork']: artwork },
          },
        } = data;

        this.pokemon.id = data.id;
        this.pokemon.name = data.name;
        this.pokemon.image = artwork.front_default;
        this.pokemon.weight = data.weight;
        this.pokemon.height = data.height;
        this.pokemon.types = data.types.map((x: any) => x.type.name);

        this.pokemon.hp = data.stats.filter(
          (x: any) => x.stat.name === 'hp'
        )[0].base_stat;

        this.pokemon.attack = data.stats.filter(
          (x: any) => x.stat.name === 'attack'
        )[0].base_stat;

        this.pokemon.specialAttack = data.stats.filter(
          (x: any) => x.stat.name === 'special-attack'
        )[0].base_stat;

        this.pokemon.defense = data.stats.filter(
          (x: any) => x.stat.name === 'defense'
        )[0].base_stat;

        this.pokemon.specialDefense = data.stats.filter(
          (x: any) => x.stat.name === 'special-defense'
        )[0].base_stat;

        this.pokemon.speed = data.stats.filter(
          (x: any) => x.stat.name === 'speed'
        )[0].base_stat;

        this.pokemon.abilities = data.abilities.map((x: any) => x.ability.name);
        this.pokemon.xp = data.base_experience;
        this.pokemon.moves = data.moves.map((x: any) => x.move.name);
      });
    }
  }
}
