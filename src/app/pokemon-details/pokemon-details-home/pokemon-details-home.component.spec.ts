import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonDetailsHomeComponent } from './pokemon-details-home.component';

describe('PokemonDetailsHomeComponent', () => {
  let component: PokemonDetailsHomeComponent;
  let fixture: ComponentFixture<PokemonDetailsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonDetailsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonDetailsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
