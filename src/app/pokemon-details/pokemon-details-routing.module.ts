import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonDetailsHomeComponent } from './pokemon-details-home/pokemon-details-home.component';

const routes: Routes = [
  {
    path: '',
    component: PokemonDetailsHomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PokemonDetailsRoutingModule {}
