import { Injectable } from '@angular/core';
import { HeaderState } from './state/header.state';
import { Observable } from 'rxjs';
import { LocalStorageService } from '../shared/services/localStorage.service';

@Injectable({
  providedIn: 'root',
})
export class HeaderFacade {
  constructor(
    private readonly headerState: HeaderState,
    private readonly localStorageService: LocalStorageService
  ) {}

  public isLoggedIn$(): Observable<boolean> {
    this.headerState.setUsername(this.localStorageService.getUsername());
    return this.headerState.getIsLoggedIn$();
  }
  public username$(): Observable<string> {
    return this.headerState.getUsername$();
  }

  public loggedOut(): void {
    localStorage.clear();
    this.headerState.setIsLoggedIn(false);
  }
}
