import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HeaderState {
  private isLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  private username$: BehaviorSubject<string> = new BehaviorSubject<string>(
    'user'
  );

  public getIsLoggedIn$(): Observable<boolean> {
    return this.isLoggedIn$;
  }

  public setIsLoggedIn(state: boolean): void {
    this.isLoggedIn$.next(state);
  }

  public getUsername$(): Observable<string> {
    return this.username$;
  }

  public setUsername(name: string): void {
    this.username$.next(name);
  }
}
