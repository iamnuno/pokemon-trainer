import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HeaderFacade } from './header.facade';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  constructor(
    private readonly router: Router,
    private readonly headerFacade: HeaderFacade
  ) {}

  get isLoggedIn$(): Observable<boolean> {
    return this.headerFacade.isLoggedIn$();
  }

  get username$(): Observable<string> {
    return this.headerFacade.username$();
  }

  public logout(): void {
    this.headerFacade.loggedOut();
    this.router.navigate(['']);
  }
}
