# Pokemon Trainer

Pokemon catalogue web app using the Angular Framework.

[PokeAPI](https://pokeapi.co/) is used to fetch Pokemons' details.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Live version

The app has been deployed on [Firebase](https://firebase.google.com/docs/hosting): https://pokemon-trainer-c6378.web.app, using the official Angular deployment [documentation](https://angular.io/guide/deployment).

## Authors

[Ahmad Abdulal](https://gitlab.com/ahab-lab) <br>
[Nuno Cunha](https://gitlab.com/iamnuno)
